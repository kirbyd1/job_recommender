############################ XPATH SELECTORS ############################

LINK_XPATHS <- list(
  page_links = '//ul[@class="pagination-list"]//a/@href',
  job_links = '//*[@id="mosaic-provider-jobcards"]/a/@href',
  job_id = '//*[@id="mosaic-provider-jobcards"]/a/@data-jk'
)
JOB_INFO_XPATHs <- list(
  title = '//*[@class="icl-u-xs-mb--xs icl-u-xs-mt--none jobsearch-JobInfoHeader-title"]',
  company = '//*[@class="icl-u-lg-mr--sm icl-u-xs-mr--xs"]//a',
  salary_and_type = '//*[@class="jobsearch-JobMetadataHeader-item  icl-u-xs-mt--xs"]',
  qualifications = '//*[@id="qualificationsSection"]//li',
  full_descr = '//*[@id="salaryInfoAndJobType"]'
)
JOB_DESCRIPTION_XPATH <- JOB_INFO_XPATHs$full_descr


############################ FUNCTION DEFINITIONS  ############################

elements_to_text <- function(response, xpath) {
  #' PUlls elements from html response and returns as text
  #' 
  #' @param response A response object returned from rvest::read_html
  #' @param xpath An xpath selector indicating which elements to extract as string
  
  return( unique(response %>% html_nodes(xpath = xpath) %>% html_text()) )
}

get_job_info <- function(response, xpath) {
  #' Modified scraper to pull job description details from Indeed job pages
  #' 
  #' @param response A job page html response from rvest::read_html
  #' @param xpath A job description xpath selector
  
  if(xpath == JOB_DESCRIPTION_XPATH) {
    # handle job description differently
    info <- response %>% html_nodes(xpath = xpath) %>% html_nodes(xpath = '//li') %>% html_text()
  } else {
    info <- elements_to_text(response, xpath)
  }
  
  return(info)
}

get_full_job_description <- function(job_link, job_id) {
  #' Driver function to scrape job description data from job posting link
  #' 
  #' @param job_link the url for the job page
  #' @param job_id the job id to associate with the job
  
  job_desc <- read_html(job_link)
  job_info <- lapply(X = JOB_INFO_XPATHs, FUN = get_job_info, response = job_desc)
  job_info$id <- job_id
  job_info$link <- job_link
  
  return(job_info)
}